
const state = {
  cartData: []
}

const getters = {
  cart: state => {
    return state.cartData
  },
  itemsInCart: state => {
    return state.cartData.map(item => item.qty).reduce((sum, currentNum) => sum + currentNum, 0)
  }
}

const mutations = {
  addToCart (state, item) {
    if (state.cartData.map(data => data.id).includes(item.id)) {
      let idx = state.cartData.map(data => data.id).indexOf(item.id)
      state.cartData[idx].qty += item.qty
    } else {
      state.cartData.push(item)
    }
  },
  removeItem (state, index) {
    state.cartData.splice(index, 1)
  },
  clearCart (state) {
    state.cartData = []
  }
}

const actions = {
  addToCart ({ commit }, item) {
    commit('addToCart', item)
  },
  removeItem ({ commit }, index) {
    commit('removeItem', index)
  },
  clearCart ({ commit }) {
    commit('clearCart')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
