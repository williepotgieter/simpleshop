
const state = {
  products: [
    {
      id: '1',
      name: 'Apple',
      shortDescription: 'Apples from South Africa',
      fullDescription: `The apple tree (Malus domestica) is a tree that grows fruit 
                        (such as apples) in the rose family best known for its juicy, 
                        tasty fruit. It is grown worldwide as a fruit tree. It is considered 
                        to be a worldwide low-cost fruit harvest-able all over the world.`,
      price: 2.00,
      image: 'apples.jpg'
    },
    {
      id: '2',
      name: 'Banana',
      shortDescription: 'Bananas from Brazil',
      fullDescription: `A banana is the common name for a type of fruit and also the name for 
                        the herbaceous plants that grow it. These plants belong to the genus 
                        Musa. They are native to the tropical region of southeast Asia. It is 
                        thought that bananas were grown for food for the first time in 
                        Papua New Guinea.`,
      price: 3.00,
      image: 'bananas.jpg'
    },
    {
      id: '3',
      name: 'Coconut',
      shortDescription: 'Coconuts from Thailand',
      fullDescription: `The coconut palm is a palm tree in the family Arecaceae (palm family). 
                        It is a large palm, growing to 30 m tall. It has leaves that are 4–6 m 
                        long. The term coconut refers to the fruit of the coconut palm. The 
                        coconut tree is a monocot.`,
      price: 4.00,
      image: 'coconuts.jpg'
    }
  ]
}

const getters = {
  productData: state => {
    return state.products
  },
  productDetail: (state) => (id) => {
    return state.products.find(item => item.id === id)
  }
}

export default {
  namespaced: true,
  state,
  getters
}
